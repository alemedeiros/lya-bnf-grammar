# Lya BNF Grammar
## Notation

```
{ ... }*   repetition possibly zero times
{ ... }+   repetition at least once
[ ... ]    optional
... | ...  alternative
```

## Reserved words

```
ARRAY, BY, CHARS, DCL, DO, DOWN, ELSE, ELSIF, END, EXIT, FI,
FOR, IF, IN, LOC, TYPE, OD, PROC, REF, RESULT, RETURN, RETURNS,
SYN, THEN, TO, WHILE,
```

## Predefined words

```
BOOL, CHAR, FALSE, INT, LENGTH, LOWER, NULL, NUM,
PRED, PRINT, READ, SUCC, TRUE, UPPER,
```

## Program
General program structure

```
<program> ::= { <statement> }+
<statement> ::= <declaration_statement>
             |  <synonym_statement>
             |  <newmode_statement>
             |  <procedure_statement>
             |  <action_statement>
```

## Declaration statement
Variable declarations

```
<declaration_statement> ::= DCL <declaration_list> ;
<declaration_list> ::= <declaration> { , <declaration> }*
<declaration> ::= <identifier_list> <mode> [ <initialization> ]
<initialization> ::= = <expression>
<identifier_list> ::= <identifier> { , <identifier> }*
<identifier> ::= [a-zA-Z_][a-zA-Z_0-9]*
```

## Synonym statement
Constant declarations

```
<synonym_statement> ::= SYN <synonym_list> ;
<synonym_list> ::= <synonym_definition> { , <synonym_definition> }*
<synonym_definition> ::= <identifier_list> [ <mode> ] = <expression>
```

## New mode statement
New type definitions

NOTES:
- looks like you can declare multiple types in the same line, all you need to do is use a comma separated list
- `<mode>` is the type
- `<element_mode>` in `<array_mode>` is the type of the array
- `<mode_name>` is an identifier for user defined modes
- `<discrete_range_mode>` is used to define a type with fewer values available, e.g. integers between 1 and 10
- multiple indexes on an array declaration is the way to declare matrices
- `<discrete_mode>` can be used as index to the array, e.g. an array indexed by characters

```
<newmode_statement> ::= TYPE <newmode_list> ;
<newmode_list> ::= <mode_definition> { , <mode_definition> }*
<mode_definition> ::= <identifier_list> = <mode>
<mode> ::= <mode_name>
        |  <discrete_mode>
        |  <reference_mode>
        |  <composite_mode>
<discrete_mode> ::= INT
                 |  BOOL
                 |  CHAR
                 |  <discrete_range_mode>
<discrete_range_mode> ::= <discrete_mode_name> ( <literal_range> )
                       |  <discrete_mode> ( <literal_range> )
<mode_name> ::= <identifier>
<discrete_mode_name> ::= <identifier>
<literal_range> ::= <lower_bound> : <upper_bound>
<lower_bound> ::= <expression>
<upper_bound> ::= <expression>
<reference_mode> ::= REF <mode>
<composite_mode> ::= <string_mode> | <array_mode>
<string_mode> ::= CHARS LBRACKET <string_length> RBRACKET
<string_length> ::= <integer_literal>
<array_mode> ::= ARRAY LBRACKET <index_mode> { , <index_mode> }* RBRACKET <mode>
<index_mode> ::= <discrete_mode> | <literal_range>
```

## Assignment statements
NOTES:
- `<dereferenced_reference>` seems like `var->`

Problems:
- why strings are always indexed by `<integer_expression>` and arrays by `<expression>`

```
<location> ::= <location_name>
            |  <dereferenced_reference>
            |  <string_element>
            |  <string_slice>
            |  <array_element>
            |  <array_slice>
            |  <call_action>
<location_name> ::= <identifier>
<dereferenced_reference> ::= <location> ->
<string_element> ::= <string_location> LBRACKET <start_element> RBRACKET
<start_element> ::= <integer_expression>
<string_slice> ::= <string_location> LBRACKET <left_element> : <right_element> RBRACKET
<string_location> ::= <identifier>
<left_element> ::= <integer_expression>
<right_element> ::= <integer_expression>
<array_element> ::= <array_location> LBRACKET <expression_list> RBRACKET
<expression_list> ::= <expression> { , <expression> }*
<array_slice> ::= <array_location> LBRACKET <lower_bound> : <upper_bound> RBRACKET
<array_location> ::= <identifier>
```

## Literals
NOTES:
- `<character_literal>` may be a plain character (`'<character>'`) or its ASCII value (`'^( <integer_literal> )''`)

```
<primitive_value> ::= <literal>
                   |  <parenthesized_expression>
<literal> ::= <integer_literal>
           |  <boolean_literal>
           |  <character_literal>
           |  <empty_literal>
           |  <character_string_literal>
<integer_literal> ::= ICONST
<boolean_literal> ::= FALSE | TRUE
<character_literal> ::= '<character>' | '^( <integer_literal> )'
<empty_literal> ::= NULL
<character_string_literal> ::= " { <character> }* "
```

## Expressions

```
<parenthesized_expression> ::= ( <expression> )
<expression> ::= <operand0> |  <conditional_expression>
<conditional_expression> ::= IF <expression> <then_expression> <else_expression> FI
                          |  IF <expression> <then_expression> <elsif_expression> <else_expression> FI
<then_expression> ::= THEN <expression>
<else_expression> ::= ELSE <expression>
<elsif_expression> ::= ELSIF <expression> <then_expression>
                    |  <elsif_expression> ELSIF <expression> <then_expression>
```

## Operands

```
<operand0> ::= <operand1>
            |  <operand0> <operator1> <operand1>
<operator1> ::= <relational_operator>
             |  <membership_operator>
<relational_operator> ::= && | || | == | != | > | >= | < | <=
<membership_operator> ::= IN
<operand1> ::= <operand2>
            |  <operand1> <operator2> <operand2>
<operator2> ::= <arithmetic_additive_operator>
             |  <string_concatenation_operator>
<arithmetic_additive_operator> ::= + | -
<string_concatenation_operator> ::= &
<operand2> ::= <operand3>
            |  <operand2> <arithmetic_multiplicative_operator> <operand3>
<arithmetic_multiplicative_operator> ::=  ∗ | / | %
<operand3> ::= [ <monadic_operator> ] <operand4>
<monadic_operator> ::= - | !
<operand4> ::= <location> | <referenced_location> | <primitive_value>
<referenced_location> ::= -> <location>
```

## General action statements

```
<action_statement> ::= [ <label_id> : ] <action> ;
<label_id> ::= <identifier>
<action> ::= <bracketed_action>
          |  <assignment_action>
          |  <call_action>
          |  <exit_action>
          |  <return_action>
          |  <result_action>
<bracketed_action> ::= <if_action> | <do_action>
<assignment_action> ::= <location> <assigning_operator> <expression>
<assigning_operator> ::= [ <closed_dyadic_operator> ] =
<closed_dyadic_operator> ::= <arithmetic_additive_operator>
                          |  <arithmetic_multiplicative_operator>
                          |  <string_concatenation_operator>
```

## Flux control
NOTE:
- It's `<action_statement>` on purpose, to simplify variable scope (probably it wouldn't add much complexity, since there may be recursive procedures)

```
<if_action> ::= IF <boolean_expression> <then_clause> [ <else_clause> ] FI
<then_clause> ::= THEN { <action_statement> }*
<else_clause> ::= ELSE { <action_statement> }*
               |  ELSIF <boolean_expression> <then_clause> [ <else_clause> ]
```

## Loops

```
<do_action> ::= DO [ <control_part> ; ] { <action_statement> }* OD
<control_part> ::= <for_control> [ <while_control> ]
                |  <while_control>
<for_control> ::= FOR <iteration>
<iteration> ::= <step_enumeration> | <range_enumeration>
<step_enumeration> ::= <loop_counter> = <start_value> [ <step_value> ] [ DOWN ] <end_value>
<loop_counter> ::= <identifier>
<start_value> ::= <discrete_expression>
<step_value> ::= BY <integer_expression>
<end_value> ::= TO <discrete_expression>
<range_enumeration> ::= <loop_counter> [ DOWN ] IN <discrete_mode_name>
<while_control> ::= WHILE <boolean_expression>
```

## Procedure calling and return
NOTES:
- `<result_action>` stores a value o the return variable, while `<return_action>` returns from the procedure, optionally changing the return value.

Problems:
- what exactly is an `<exit_action>`? A GOTO-like action?

```
<call_action> ::= <procedure_call> | <builtin_call>
<procedure_call> ::= <procedure_name> ( [ <parameter_list> ] )
<parameter_list> ::= <parameter> { , <parameter> }*
<parameter> ::= <expression>
<exit_action> ::= EXIT label_id
<return_action> ::= RETURN [ <result> ]
<result_action> ::= RESULT <result>
<result> ::= <expression>
<builtin_call> ::= <builtin_name> ( [ <parameter_list> ] )
<builtin_name> ::= NUM | PRED | SUCC | UPPER | LOWER | LENGTH | READ | PRINT
```

## Procedure declaration
NOTES:
- `LOC` is like C++ reference
- procedure may have no statements

```
<procedure_statement> ::= <label_id> : <procedure_definition> ;
<procedure_definition> ::= PROC ( [ <formal_parameter_list> ] ) [ <result_spec> ]; { <statement> }* END
<formal_parameter_list> ::= <formal_parameter> { , <formal_parameter> }*
<formal_parameter> ::= <identifier_list> <parameter_spec>
<parameter_spec> ::= <mode> [ <parameter_attribute> ]
<parameter_attribute> ::= LOC
<result_spec> ::= RETURNS ( <mode> [ <result_attribute> ] )
<result_attribute>::= LOC
```

## Comments
NOTES:
- Comments are ignored during Lexer tokenizing step

```
<comment> ::= <bracketed_comment> | <line_end_comment>
<bracketed_comment> ::= /_ <character_string> _/
<line_end_comment> ::= // <character_string> <end_of_line>
<character_string> ::= { <character> }*
```
